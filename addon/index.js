import { tracked } from '@glimmer/tracking';

class Dummy {
  @tracked value;
}

export default function trackify(obj, attr) {
  const dummy = new Dummy();
  dummy.value = obj[attr];
  Object.defineProperty(obj, attr, {
    get() {
      return dummy.value;
    },
    set(val) {
      dummy.value = val;
    },
  })
}
